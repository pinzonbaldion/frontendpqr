import { TestBed } from '@angular/core/testing';

import { PqrInterceptorService } from './pqr-interceptor.service';

describe('PqrInterceptorService', () => {
  let service: PqrInterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PqrInterceptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
