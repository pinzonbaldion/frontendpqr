export class Pqr {
    id?: number;
	codPqr: string;
	tipo: string;
    asunto: string;
	usuario: string;
	estado: string;
	fecCre: Date;
    fecLimt: Date;
    
    constructor(codPqr: string,tipo: string,asunto: string,usuario: string,estado: string,fecCre: Date,fecLimt: Date){
        this.codPqr = codPqr;
        this.tipo = tipo;
        this.asunto = asunto;
        this.usuario = usuario;
        this.estado = estado;
        this.fecCre = fecCre;
        this.fecLimt = fecLimt;
    }

}
