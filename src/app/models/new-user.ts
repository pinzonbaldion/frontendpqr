export class NewUser {
    names: string;
    userName: string;
    email: string;
    password: string;
    constructor(names: string, userName: string, email: string, password: string) {
        this.names = names;
        this.userName = userName;
        this.email = email;
        this.password = password;
    }
}
