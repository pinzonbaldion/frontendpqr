import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DetailPqrComponent } from './pqr/detail-pqr.component';
import { UpdatePqrComponent } from './pqr/update-pqr.component';
import { ListPqrComponent } from './pqr/list-pqr.component';
import { HomeComponent } from './home/home/home.component';
import { IndexComponent } from './index/index.component';


const routes: Routes = [
  {path: '',component: IndexComponent},
  {path: 'login',component: LoginComponent},
  {path: 'pqrList',component: ListPqrComponent},
  {path: 'pqrDetail/:id',component: DetailPqrComponent},
  {path: 'pqrUpdate/:id',component: UpdatePqrComponent},
  {path: 'home',component: HomeComponent},
  {path: '**', redirectTo: 'pqr/pqrList', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
