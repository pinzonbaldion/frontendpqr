import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pqr } from '../models/pqr';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PqrService {

  pqrURL = 'http://localhost:8887/pqr/';

  constructor(private httpClient: HttpClient) { }

  public list(): Observable<Pqr[]>{
    return this.httpClient.get<Pqr[]>(this.pqrURL + 'pqrList');
  }

  public detail(id: number): Observable<Pqr>{
    return this.httpClient.get<Pqr>(this.pqrURL + `pqrDetail/${id}`);
  }

  public detailCodPqr(codPqr: string): Observable<Pqr>{
    return this.httpClient.get<Pqr>(this.pqrURL + `pqrDetailCod/${codPqr}`);
  }

  public save(pqr: Pqr): Observable<any>{
    return this.httpClient.post<any>(this.pqrURL + 'pqrCreate',pqr);
  }

  public update(id: number, pqr: Pqr): Observable<any>{
    return this.httpClient.put<any>(this.pqrURL + `pqrUpdate/${id}`, pqr);;
  }

  public delete(id: number): Observable<any>{
    return this.httpClient.delete<any>(this.pqrURL + `pqrDelete/${id}`);
  }

}
