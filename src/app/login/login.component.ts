import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Login } from '../models/login';
import { TokenService } from '../services/token.service';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { NewUser } from '../models/new-user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  container:any;
  //Datos para registrar
  newUser: NewUser;
  names: string;
  userNameNew: string;
  passwordNew: string;
  email: string;
  //Datos para login
  isLogged = false;
  isLoginFail = false;
  login: Login;
  userName: string;
  password: string;
  roles: string[] = [];
  errMsj: string;

  

  constructor(
    private tokenService: TokenService,
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    if (this.tokenService.getToken()) {
      this.isLogged = true;
      this.isLoginFail = false;
      this.roles = this.tokenService.getAuthorities();
    }
  }

  onLogin(): void {
    this.login = new Login(this.userName, this.password);
    this.authService.login(this.login).subscribe(
      data => {
        this.isLogged = true;
        this.tokenService.setToken(data.token);
        this.tokenService.setUserName(data.userName);
        this.tokenService.setAuthorities(data.authorities);
        this.roles = data.authorities;
        this.toastr.success('Bienvenido ' + data.userName, 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigateByUrl('/home');
      },
      err => {
        this.isLogged = false;
        this.errMsj = err.error;
        this.toastr.error(this.errMsj, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
      }
    );
  }

  onRegister(): void {
    this.newUser = new NewUser(this.names, this.userNameNew, this.email, this.passwordNew);
    this.authService.new(this.newUser).subscribe(
      data => {
        this.toastr.success('Usuario Registrado', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });

        this.router.navigate(['/login']);
        //this.transition("registerSucces");
      },
      err => {
        this.errMsj = err.error.mensaje;
        this.toastr.error(this.errMsj, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
        // console.log(err.error.message);
      }
    );
  }


  transition(value: any){
    this.container = document.getElementById('container');
    if (value == "signIn") {
      this.container.classList.remove("right-panel-active");
    } else if(value == "signUp"){
      this.container.classList.add("right-panel-active");
    }
  }

}
