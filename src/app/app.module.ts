import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ListPqrComponent } from './pqr/list-pqr.component';
import { DetailPqrComponent } from './pqr/detail-pqr.component';
import { UpdatePqrComponent } from './pqr/update-pqr.component';
import { ModalComponent } from './modal/modal/modal.component';
import { HomeComponent } from './home/home/home.component';
import { IndexComponent } from './index/index.component';
import { interceptorProvider } from './interceptor/pqr-interceptor.service';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

//Externals
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { DatePipe } from '@angular/common';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListPqrComponent,
    DetailPqrComponent,
    UpdatePqrComponent,
    ModalComponent,
    HomeComponent,
    IndexComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [interceptorProvider,DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
