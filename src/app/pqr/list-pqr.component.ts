import { Component, OnInit } from '@angular/core';
import { Pqr } from '../models/pqr';
import { PqrService } from '../services/pqr.service';
import { ToastrService } from 'ngx-toastr';
import { TokenService } from '../services/token.service';

@Component({
  selector: 'app-list-pqr',
  templateUrl: './list-pqr.component.html',
  styleUrls: ['./list-pqr.component.scss']
})
export class ListPqrComponent implements OnInit {

  pqr: Pqr[] = [];
  roles: string[];
  isAdmin = false;
  constructor(
    private pqrService: PqrService,
    private toastr: ToastrService,
    private tokenService: TokenService
    ) { }

  ngOnInit(): void {
    this.listPqr();
    this.roles = this.tokenService.getAuthorities();
    this.roles.forEach(rol => {
      if (rol === 'ROLE_ADMIN') {
        this.isAdmin = true;
      }
    });
  }

  listPqr(): void{
    this.pqrService.list().subscribe(
      data => {
        this.pqr = data;
      },
      err => {
        console.log(err);
      } 
    );
  }

  deletePqr(id: number) {
    if (confirm("Esta seguro que desea eliminar la PQR?")) {
      this.pqrService.delete(id).subscribe(
        data => {
          this.toastr.success('PQR Eliminada', 'OK', {
            timeOut: 3000, positionClass: 'toast-top-center'
          });
          this.listPqr();
        },
        err => {
          this.toastr.error(err.error.mensaje, 'Fail', {
            timeOut: 3000, positionClass: 'toast-top-center',
          });
        }
      );
    }
  }

}
