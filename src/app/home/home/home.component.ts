import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isLogged = false;
  userName = '';

  constructor(private tokenService: TokenService, private route: Router) { }

  ngOnInit() {
    if (this.tokenService.getToken()) {
      this.isLogged = true;
      this.userName = this.tokenService.getUserName();
    } else {
      this.isLogged = false;
      this.userName = '';
    }
  }

  onLogOut(): void {
    this.tokenService.logOut();
    //window.location.reload();
    this.route.navigate(['/login']);
  }

}
