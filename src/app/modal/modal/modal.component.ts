import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PqrService } from 'src/app/services/pqr.service';
import { Pqr } from 'src/app/models/pqr';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { stringify } from '@angular/compiler/src/util';
import { TokenService } from 'src/app/services/token.service';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  pqr: Pqr[] = [];
  codPqr: string;
  tipo: string;
  asunto: string;;
  usuario: string = this.tokenService.getUserName();
  estado: string;
  fecCre: Date = new Date();
  fecLimt: Date = new Date();
  isMsj: string;


  constructor(
    private pqrService: PqrService,
    private location: Location,
    private toastr: ToastrService,
    private router: Router,
    private cd: ChangeDetectorRef,
    private tokenService: TokenService
  ) { }

  ngOnInit(): void {
  }

  calculateFecLimt() {
    /*se suma un dia adicional a los establecidos por tema de DATE
    debido a que el toma el dia de la creacion coomo un dia por
    eso se un dia de mas*/
    switch (this.tipo) {
      case "P":
        this.fecLimt.setDate(this.fecCre.getDate() + 8);
        break;
      case "Q":
        this.fecLimt.setDate(this.fecCre.getDate() + 4);
        break;
      case "R":
        this.fecLimt.setDate(this.fecCre.getDate() + 3);
        break;
      default:
        break;
    }
  }

  formatearFecha(fecha: any) {
    if (fecha instanceof Date) {
      const dia: string = fecha.getDate().toString().padStart(2, "0");
      const mes: string = (fecha.getMonth() + 1).toString().padStart(2, "0");
      const anno: string = fecha.getFullYear().toString();
      return `${dia}/${mes}/${anno}`
    }
    return fecha;
  }

  listPqr(): void {
    this.pqrService.list().subscribe(
      data => {
        this.pqr = data;
      },
      err => {
        console.log(err);
      }
    );
  }

  savePqr(): void {
    this.usuario = this.tokenService.getUserName();
    console.log(this.tokenService.getUserName());
    this.calculateFecLimt();
    const pqr = new Pqr(this.codPqr, this.tipo, this.asunto, this.usuario, this.estado, this.fecCre, this.fecLimt);
    this.pqrService.save(pqr).subscribe(
      data => {
        this.toastr.success('PQR Creada', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.listPqr();
      },
      err => {
        console.log("Error + " + err.error.mensaje);
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000, positionClass: 'toast-top-center',
        });
      }
    );
  }
}


